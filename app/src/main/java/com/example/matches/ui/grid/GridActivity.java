package com.example.matches.ui.grid;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.matches.R;
import com.example.matches.interfaces.GameActions;
import com.example.matches.interfaces.GridActions;
import com.example.matches.model.CardItem;


public class GridActivity extends AppCompatActivity implements GameActions {

    public static final String DIM_1_VALUE = "dim_1_value";
    public static final String DIM_2_VALUE = "dim_2_value";
    private TextView tvMatchesDone;
    private int matches=0;
    GridFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        fragment= GridFragment.newInstance(getIntent().getIntExtra(DIM_1_VALUE,4),getIntent().getIntExtra(DIM_2_VALUE,4));


        getSupportFragmentManager().beginTransaction().replace(R.id.content,fragment).commit();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvMatchesDone= (TextView) this.findViewById(R.id.tvDoneMatches);

        tvMatchesDone.setText(getString(R.string.done_matches)+ String.valueOf(matches));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grid, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_reset) {
            fragment.setGame();
        }

        return  super.onOptionsItemSelected(item);
    }


    @Override
    public void matchDone() {

        matches++;
        tvMatchesDone.setText(getString(R.string.done_matches)+String.valueOf(matches));



    }
}
