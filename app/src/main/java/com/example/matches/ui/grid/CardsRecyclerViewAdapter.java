package com.example.matches.ui.grid;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.matches.R;
import com.example.matches.interfaces.GridActions;
import com.example.matches.model.CardItem;
import com.squareup.picasso.Picasso;


import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link CardItem} and makes a call to the
 * specified {@link GridActions}.
 * TODO: Replace the implementation with code for your data type.
 */
public class CardsRecyclerViewAdapter extends RecyclerView.Adapter<CardsRecyclerViewAdapter.CardItemViewHolder> {

    private final List<CardItem4Adapter> mValues;
    private final GridActions mListener;
    private final Context mCtx;
    private int flipped=0;

    public CardsRecyclerViewAdapter(Context ctx, List<CardItem4Adapter> items, GridActions listener) {
        mValues = items;
        mListener = listener;
        this.mCtx=ctx;
    }

    @Override
    public CardItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card, parent, false);
        return new CardItemViewHolder(view);
    }


    public void clearFlipped()
    {
        flipped=0;
    }

    @Override
    public void onBindViewHolder(final CardItemViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);


        if(holder.mItem.isMatched())
        {
            holder.mRlMatched.setBackgroundResource(R.color.matched);
        }
        else
            holder.mRlMatched.setBackgroundResource(0);

        if(holder.mItem.isShowing())
        {
            Picasso.with(mCtx).load(Uri.parse(mValues.get(position).mItem.getUrl())).into(holder.mPic);
        }
        else
           holder.mPic.setImageResource(R.mipmap.ic_launcher);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(holder.mItem.isMatched())
                    return;

                if(!holder.mItem.isShowing() && flipped< 2)
                {
                    Picasso.with(mCtx).load(Uri.parse(mValues.get(position).mItem.getUrl())).into(holder.mPic);
                    flipped++;
                    holder.mItem.setShowing(!holder.mItem.isShowing());

                    if (null != mListener) {

                        mListener.onItemClick(holder.mItem);

                    }

                }
                else if(flipped>0 && holder.mItem.isShowing()){
                    holder.mPic.setImageResource(R.mipmap.ic_launcher);
                    flipped--;

                    holder.mItem.setShowing(!holder.mItem.isShowing());


                    if (null != mListener) {

                        mListener.onItemClick(holder.mItem);

                    }
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class CardItemViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mPic;
        public final RelativeLayout mRlMatched;



        public CardItem4Adapter mItem;

        public CardItemViewHolder(View view) {
            super(view);
            mView = view;
            mPic= (ImageView) view.findViewById(R.id.ivPic);
            mRlMatched=(RelativeLayout)view.findViewById(R.id.rlMatched);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mItem.toString() + "'";
        }


    }
}
