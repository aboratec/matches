package com.example.matches.ui.main;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.matches.R;
import com.example.matches.ui.grid.GridActivity;


public class MainActivity extends AppCompatActivity {

    private EditText etDim1,etDim2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etDim1=(EditText)findViewById(R.id.etDim1);
        etDim2=(EditText)findViewById(R.id.etDim2);
    }



    public void goGrid(View v)
    {




        if (v != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }


        Intent gridIntent= new Intent(this, GridActivity.class);

        int dim1=4;
        int dim2=4;




        try
        {
            dim1=Integer.parseInt(etDim1.getText().toString());
        }
        catch (Exception ex)
        {

        }

        try
        {
            dim2=Integer.parseInt(etDim2.getText().toString());
        }
        catch (Exception ex)
        {

        }


        if((dim1*dim2)%2!=0)
        {
            Snackbar.make(etDim1,R.string.not_pair,Snackbar.LENGTH_LONG).show();
            return;
        }

        gridIntent.putExtra(GridActivity.DIM_1_VALUE,dim1);
        gridIntent.putExtra(GridActivity.DIM_2_VALUE,dim2);


        startActivity(gridIntent);

    }
}
