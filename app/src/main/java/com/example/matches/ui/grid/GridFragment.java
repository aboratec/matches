package com.example.matches.ui.grid;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.matches.R;
import com.example.matches.interfaces.GameActions;
import com.example.matches.interfaces.GridActions;
import com.example.matches.managers.ImagesManager;
import com.example.matches.model.CardItem;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link GridActions}
 * interface.
 */
public class GridFragment extends Fragment implements GridActions{

    // TODO: Customize parameter argument names
    private static final String ARG_DIM_1 = "dim1";
    private static final String ARG_DIM_2 = "dim2";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private GameActions mListener;
    private List<CardItem> mList;
    private int dim1=4,dim2=4;
    private CardsRecyclerViewAdapter adapter;
    private ArrayList<CardItem4Adapter> mList4Adapter;
    private RecyclerView recyclerView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GridFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static GridFragment newInstance(int dim1,int dim2) {
        GridFragment fragment = new GridFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_DIM_1, dim1);
        args.putInt(ARG_DIM_2, dim2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            dim1 = getArguments().getInt(ARG_DIM_1);
            dim2 = getArguments().getInt(ARG_DIM_2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid, container, false);





        mColumnCount=dim2;


        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }



        }

        setGame();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GameActions) {
            mListener = (GameActions) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement GridActions");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    HashMap<Integer,CardItem4Adapter> flippedItems=new HashMap<Integer,CardItem4Adapter>();


    public void setGame()
    {
        ImagesManager manager= new ImagesManager();

        mList= manager.generateItems(dim1,dim2);

        mList4Adapter= new ArrayList<CardItem4Adapter>();

        for(CardItem item : mList)
        {
            CardItem4Adapter newItem= new CardItem4Adapter();

            newItem.mItem=item;

            mList4Adapter.add(newItem);
        }

        adapter=new CardsRecyclerViewAdapter(getActivity(),mList4Adapter, this);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(CardItem4Adapter item)
    {

        if(item.isShowing())
            flippedItems.put(item.mItem.getId(),item);
        else
            flippedItems.remove(item.mItem.getId());


        if(flippedItems.size()==2)
        {
            final CardItem4Adapter[] pair= new CardItem4Adapter[2];

            flippedItems.values().toArray(pair);

            //Check if pair
            if(pair[0].mItem.isTheSameAs(pair[1].mItem))
            {
                pair[0].setMatched(true);
                pair[1].setMatched(true);
                mListener.matchDone();

                flippedItems.clear();
                adapter.clearFlipped();
                adapter.notifyDataSetChanged();

            }
            else //if not, flip over again
            {

                Handler handler= new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pair[0].setMatched(false);
                        pair[1].setMatched(false);
                        pair[0].setShowing(false);
                        pair[1].setShowing(false);

                        flippedItems.clear();
                        adapter.clearFlipped();
                        adapter.notifyDataSetChanged();
                    }
                },1000);


            }





        }


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
