package com.example.matches.ui.grid;

import com.example.matches.model.CardItem;

/**
 * Created by Miguel on 03/03/2017.
 */

public class CardItem4Adapter
{
    private boolean showing=false;
    private boolean matched=false;

    public CardItem mItem;

    public boolean isShowing() {
        return showing;
    }

    public void setShowing(boolean showing) {
        this.showing = showing;
    }

    public boolean isMatched() {
        return matched;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    @Override
    public String toString() {
        return super.toString() + " '" + mItem.toString() + "'";
    }
}
