package com.example.matches.interfaces;

import com.example.matches.model.CardItem;
import com.example.matches.ui.grid.CardItem4Adapter;
import com.example.matches.ui.grid.CardsRecyclerViewAdapter;

/**
 * Created by Miguel on 02/03/2017.
 */

public interface GridActions
{
    public void onItemClick(CardItem4Adapter item);

}
