package com.example.matches.model;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by Miguel on 02/03/2017.
 */

public class CardItem implements Serializable {


    private String url;
    private int id;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String serialize() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static CardItem deserialize(String serializedData) {
        if (serializedData != null) {
            Gson gson = new Gson();
            return gson.fromJson(serializedData, CardItem.class);
        } else {
            return new CardItem();
        }
    }


    public boolean isTheSameAs(CardItem item)
    {
        return this.url.compareTo(item.getUrl())==0;
    }
}
